The purpose of the spring-rest-exceptions
========================

This module contains exception classes that can be used with Spring controllers to automatically return the correct HTTP error code.
  
Example
-------

This is an example of how this is used.
 
When the ResourceNotFoundException is thrown the controller will automatically return a 404 error.
 
	@Controller
	@EnableAutoConfiguration
	public class UserControllerImpl implements UserController {
		@Autowired
		UserRepository userRepo;
	
		@Produce(uri="direct:UserModifiedEventOutboundRoute")
		OutboundEndpoint outboundEndpoint;

		/* (non-Javadoc)
		* @see eu.miman.repo.user.controller.UserController#get(java.lang.String)
		*/
		@RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
		@ResponseBody
		public User get(@PathVariable String userId) {
			User user = userRepo.findByExternalId(userId);
		
			if (user == null) {
				throw new ResourceNotFoundException();
			}
		
			return user;
		}
	}

