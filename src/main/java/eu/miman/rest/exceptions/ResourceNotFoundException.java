package eu.miman.rest.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is thrown whenever a resource isn't found and we want the controller to send back a NOT_FOUND (404) error.
 * 
 * @author Mikael Thorman
 */
@ResponseStatus(value = org.springframework.http.HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3771567219277425830L;

}
